package com.ddelizia.growth.hacks.core.configuration;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Configuration;

/**
 * @author ddelizia
 * @since 17/03/15 16:56
 */
@Configuration
public class TwitterConfiguration {

    private static final Logger LOGGER = Logger.getLogger(TwitterConfiguration.class);


    private static final String twitter_oauth_consumer_key ="..";
    private static final String twitter_oauth_token ="..";
    private static final String twitter_oauth_consumer_secret ="..";
    private static final String twitter_oauth_token_secret ="..";

}
