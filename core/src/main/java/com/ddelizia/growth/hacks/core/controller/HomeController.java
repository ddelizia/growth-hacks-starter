package com.ddelizia.growth.hacks.core.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ddelizia
 * @since 17/04/15 19:00
 */

@Controller
public class HomeController {

    @RequestMapping("/")
    String index() {
        return "index";
    }


}
