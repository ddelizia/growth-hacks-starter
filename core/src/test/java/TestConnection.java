import com.mongodb.*;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author ddelizia
 * @since 17/04/15 20:23
 */
public class TestConnection {

    @Test
    public void test() throws Exception{
        String db = "growth";
        String collection= "letsjapanBotRetweet";

        ServerAddress serverAddress = new ServerAddress("ds061671.mongolab.com",61671);
        MongoCredential mongoCredential = MongoCredential.createMongoCRCredential("ddelizia", db, "ddelizia".toCharArray());

        //String mongoClientUri = String.format("mongodb://%s:%s@%s:%s/%s","ddelizia","ddelizia","ds061671.mongolab.com","61671",db);
        //MongoClient mongoClient = new MongoClient(new MongoClientURI(mongoClientUri));
        MongoClient mongoClient = new MongoClient(
                serverAddress, Arrays.asList(mongoCredential)
        );

        mongoClient.getDB(db).getCollection(collection).save(new BasicDBObject("test","test"));

    }


}
